# Package Test

This is a test repository for building a JS program into executables using [vercel/pkg](https://github.com/vercel/pkg) and Gitlab CI/CD.
This can be used as reference for other developers wanting to do the same.

Artifacts contain a test executable (a simple "hello world" program that also prints any arguments it receives) and this README.md file.
